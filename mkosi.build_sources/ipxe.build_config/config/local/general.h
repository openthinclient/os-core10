/* disable WiFi support */
#undef CRYPTO_80211_WEP
#undef CRYPTO_80211_WPA
#undef CRYPTO_80211_WPA2

/* disable image support */
#undef IMAGE_PNG
#undef IMAGE_DER
#undef IMAGE_PEM

#define NSLOOKUP_CMD
#define CONSOLE_CMD
