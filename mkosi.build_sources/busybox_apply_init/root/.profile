#!/bin/bash
. /init_functions
PS1='\[\033[01;32m\]root\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

alias ll='ls -al'
alias DE='tar -xzOf /lib/keymaps.tar.gz de-latin1-nodeadkeys.bmap | /sbin/loadkmap'
alias FR='tar -xzOf /lib/keymaps.tar.gz fr-latin1.bmap | /sbin/loadkmap'
alias reboot='echo b > /proc/sysrq-trigger'
alias halt='echo o > /proc/sysrq-trigger'

export PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Do not apologize if we're in debug mode already
if ! grep -qF 'debug=true' /proc/cmdline; then
    echo -e "\
\t ____________________________________________________________________
\t|                                                                    |
\t| Unfortuantely, your computer could not boot the openthinclient OS. |
\t| We apologize for this inconvenience.                               |
\t|                                                                    |
\t|$WHITE Please contact your system administrator.$NORMAL                          |
\t|____________________________________________________________________|


\tShould you be the administrator, you can read our documentation at
\t\t${WHITE}https://docs.openthinclient.com${NORMAL}

\tIf you want professional support (for which we charge a service fee),
\tplease contact us at openthinclient via e-mail or phone.

\t\t${WHITE}info@openthinclient.com${NORMAL}
\t\t${WHITE}+49 711 13786360${NORMAL}


Press Return to open a debug shell.
"
    read _
    echo "${CLEAR}"
fi

echo -e "
This is a busybox ${BLUE}debug shell${NORMAL}.

Press CTRL+D to continue booting if possible. Press TAB to list all commands.
Special commands: \"DE\" or \"FR\" for german or french keyboard layout.
                  \"reboot\" or \"halt\" to restart or power off the system.
"
