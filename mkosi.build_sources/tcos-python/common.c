#define _GNU_SOURCE
#include "common.h"
#include <sys/types.h>
#include <unistd.h>
#include <grp.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <stdlib.h>

const char *ALLOWED_PREFIXES[] = {"/usr/", "/bin/", "/sbin/", "/opt/", "/lib/"};

char *dump_file_error = "";
// Helper
#define fmt(fmt, ...) ({ \
    char *buf; \
    if (0 > asprintf(&buf, fmt, ##__VA_ARGS__)) exit(ERROR_CODE); \
    buf; \
})

char *dump_file(char *path, int max_len) {
    FILE *f = fopen(path, "r");
    if (!f) {
        dump_file_error = fmt("Could not open %s: %s",path, strerror(errno));
        return NULL;
    }
    char *buf = malloc(max_len);
    if (!buf) {
        dump_file_error = "Could not allocate buffer";
        fclose(f);
        return NULL;
    }
    if (!fgets(buf, 1024, f)) {
        dump_file_error = fmt("Could not read %s: %s", path, strerror(errno));
        fclose(f);
        return NULL;
    }
    fclose(f);
    return buf;
}

// Check if ptrace is enabled for others than root.
char* ptrace_is_restricted() {
    char *scope = dump_file("/proc/sys/kernel/yama/ptrace_scope", 2);
    if (!scope) {
        return dump_file_error;
    }
    if (strcmp(scope, "0\n") == 0 || strcmp(scope, "1\n") == 0) {
        return "ptrace is enabled for non-root users.";
    }
    return NULL;
}

// Check if we are inside a user namespace.
const char *good_map = "         0          0 4294967295\n";
const int good_map_strlen = 33;
char* user_is_unmapped() {
    char *uid_map = dump_file("/proc/self/uid_map", good_map_strlen);
    char *gid_map = dump_file("/proc/self/gid_map", good_map_strlen);
    if (!uid_map || !gid_map) {
        return dump_file_error;
    }
    if (strcmp(uid_map, good_map) != 0 || strcmp(gid_map, good_map) != 0) {
        return "Running inside user namespace.";
    }
    return NULL;
}

// Check if script file is writable only by root.
char* file_is_trustworthy(char *script_path) {
    char *abs_script_path = realpath(script_path, NULL);
    if (!abs_script_path)
        return fmt("Could not resolve %s: %s", script_path, strerror(errno));

    // Check if the file is in a trustworthy directory.
    const char **prefix = ALLOWED_PREFIXES;
    int prefix_count = sizeof(ALLOWED_PREFIXES) / sizeof(ALLOWED_PREFIXES[0]);
    int trusted_dir = 0;
    for (int i = 0; i < prefix_count && !trusted_dir; i++) {
        trusted_dir = strncmp(abs_script_path, *prefix, strlen(*prefix)) == 0;
        prefix = &ALLOWED_PREFIXES[i];
    }
    if (!trusted_dir)
        return fmt("%s not trusted. (Untrustworthy dir.)", abs_script_path);

    // Check if the file is owned by root:root and not writable by others.
    struct stat file_sb;
    if (stat(abs_script_path, &file_sb) != 0)
        return fmt("Could not stat %s: %s", abs_script_path, strerror(errno));
    if (!S_ISREG(file_sb.st_mode))
        return "First argument must be a file.";
    if (file_sb.st_uid != 0 || file_sb.st_gid != 0)
        return fmt("%s not trusted. (Not owned by root.)", abs_script_path);
    if ((file_sb.st_mode & S_IWOTH) != 0)
        return fmt("%s not trusted. (Others may write.)", abs_script_path);

    // Check if the file is in a trusted mount.
    char *parent = dirname(abs_script_path);
    struct stat parent_sb;
    struct stat root_sb;
    if (stat(parent, &parent_sb) != 0)
        return fmt("Could not stat %s: %s", parent, strerror(errno));
    if (stat("/", &root_sb) != 0)
        return fmt("Could not stat /: %s", strerror(errno));
    if (parent_sb.st_dev != root_sb.st_dev)
        return fmt("%s not trusted. (Not in root mount.)", abs_script_path);

    return NULL;
}

// Add supplementary group to the current process.
char *add_suppl_group() {
    int ngroups = getgroups(0, NULL);
    gid_t groups[ngroups + 1];
    if (getgroups(ngroups, groups) != ngroups)
        return fmt("Could not get groups: %s", strerror(errno));
    groups[ngroups] = SUPPL_GID;
    if (setgroups(ngroups + 1, groups) != 0)
        return fmt("Could not setgroups: %s", strerror(errno));
    return NULL;
}

void print_usage(char *script) {
    fprintf(stdout,
            "Usage: %s <script_path> [args...]\n\n"
            "Run script with elevated privileges.\n",
            script);
}
