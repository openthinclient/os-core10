#include "common.h"
#include <unistd.h>
#include <grp.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>


#define BASH_PATH "/bin/bash"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        print_usage(argv[0]);
        return ERROR_CODE;
    }
    if (argv[1][0] == '-') {
        fprintf(stderr, "Options are not supported.\n");
        print_usage(argv[0]);
        return ERROR_CODE;
    }

    char *err = ptrace_is_restricted();
    err || (err = user_is_unmapped());
    err || (err = file_is_trustworthy(argv[1]));
    if (err) {
        fprintf(stderr, "tcos-bash: %s\n", err);
        return ERROR_CODE;
    }
    err = add_suppl_group();
    if (err) {
        fprintf(stderr, "tcos-bash: %s\n", err);
        return ERROR_CODE;
    }

    argv[0] = BASH_PATH;
    execvp(BASH_PATH, argv);
    perror("tcos-bash: execvp failed!");
    return ERROR_CODE;
}
