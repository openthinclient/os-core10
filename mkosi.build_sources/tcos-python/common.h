#include <sys/types.h>

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define SUPPL_GID 555
#define ERROR_CODE 126

char *ptrace_is_restricted();
char *user_is_unmapped();
char *file_is_trustworthy(char *script_path);
char *add_suppl_group();
void print_usage(char *script);

#endif
