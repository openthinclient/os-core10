#include "common.h"
#include <unistd.h>
#include <sys/types.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char *argv[]) {
    int i;

    if (argc < 2) {
        fprintf(stderr,
                "Usage: %s program [args...]\n\n"
                "Run program with elevated privileges removed.\n",
                argv[0]);
        return ERROR_CODE;
    }

    // Get list of supplementary groups
    int ngroups = getgroups(0, NULL);
    if (ngroups < 1) {  // error or no groups
        perror("getgroups");
        return ERROR_CODE;
    }

    gid_t groups[ngroups];
    ngroups = getgroups(ngroups, groups);
    if (ngroups == -1) {
        perror("getgroups");
        return ERROR_CODE;
    }

    // Find the supplementary group in the list
    for(i = 0; i < ngroups; i++) {
        if (groups[i] == SUPPL_GID) {
            break;
        }
    }
    if (i < ngroups) {
        // Remove the supplementary group by shifting the rest of the list
        for (; i < ngroups - 1; i++) {
            groups[i] = groups[i + 1];
        }
        // Set the new list of supplementary groups
        if (setgroups(ngroups - 1, groups) == -1) {
            perror("setgroups");
            return ERROR_CODE;
        }
    }

    // Skip the first argument "tcos-drop-tcosd"
    argv++;

    execvp(argv[0], &argv[0]);
    perror("execvp");
    return ERROR_CODE;
}
