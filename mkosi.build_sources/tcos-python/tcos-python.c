#include "common.h"
#include <unistd.h>
#include <grp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>


#define PYTHON_PATH "/usr/bin/python3"

int main(int argc, char *argv[]) {
    if (argc < 2) {
        print_usage(argv[0]);
        return ERROR_CODE;
    }
    if (argv[1][0] == '-') {
        fprintf(stderr, "Options are not supported.\n");
        print_usage(argv[0]);
        return ERROR_CODE;
    }

    char *err = ptrace_is_restricted();
    err || (err = user_is_unmapped());
    err || (err = file_is_trustworthy(argv[1]));
    if (err) {
        fprintf(stderr, "tcos-python: %s\n", err);
        return ERROR_CODE;
    }

    err = add_suppl_group();
    if (err) {
        fprintf(stderr, "tcos-python: %s\n", err);
        return ERROR_CODE;
    }


    char **new_argv = malloc((argc + 2) * sizeof(char *));
    if (!new_argv) {
        perror("tcos-python: Could not allocate new_argv");
        return ERROR_CODE;
    }
    new_argv[0] = PYTHON_PATH;
    new_argv[1] = "-Es"; // ignore PYTHON* env vars, disable user site-packages
    for (int i = 1; i < argc; i++) {
        new_argv[i + 1] = argv[i];
    }
    new_argv[argc + 1] = NULL;

    execvp(PYTHON_PATH, new_argv);
    perror("tcos-python: execvp failed!");
    return ERROR_CODE;
}
