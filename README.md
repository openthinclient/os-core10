# os-core10

Test build of openthinclient OS with mkosi

- [os-core10](#os-core10)
  * [Building](#building)
    + [Building using cached images](#building-using-cached-images)
    + [Building from scratch](#building-from-scratch)
    + [GIT LFS](#git-lfs)
  * [Workflow](#workflow)

## Building

Install dependencies on Debian-based systems with
```bash
sudo apt install git debootstrap systemd-container fakeroot lzop dpkg dpkg-dev squashfs-tools git-lfs zsync
```
For current Debian Buster a newer `squashfs-tools` version >= 4.4 is necessary:
```bash
curl -O http://ftp.de.debian.org/debian/pool/main/s/squashfs-tools/squashfs-tools_4.4-2_amd64.deb
dpkg -i squashfs-tools_4.4-2_amd64.deb
```

On RedHat-based systems:
```bash
sudo dnf install git debootstrap systemd-container fakeroot lzop dpkg squashfs-tools git-lfs dpkg-dev apt apt-utils
```
For RedHat-based systems - at least Fedora - you additionally need to manually install `zsync`:
```bash
curl -LO http://zsync.moria.org.uk/download/zsync-0.6.2.tar.bz2
tar xjf zsync-0.6.2.tar.bz2
cd zsync-0.6.2
./configure
make
sudo make install
```

On archlinux:
```bash
yay -S apt-git cpio debhelper debootstrap dpkg fakeroot git git-lfs lzop squashfs-tools zsync
```

To use the incremental mode (-i) you have to use [our mkosi fork](https://github.com/openthinclient/mkosi) at the moment. So start by checking it out and installing it locally.
```bash
git clone https://github.com/openthinclient/mkosi
cd mkosi
python3 -m pip install --user --editable .
cd ..
```
Clone the os-core10 repo with (see [GIT LFS](#markdown-header-git-lfs) for details)
```bash
git clone --recursive https://bitbucket.org/openthinclient/os-core10
```
to get busybox and freerdp submodule directly. Or use
```bash
git submodule update --init
```
inside the repository if you have cloned the repository without the `--recursive`parameter.

### Building using cached images

If you want to make sure, that you have the exact same image as intended unpack base.sfs.cache.tar.gz:
```bash
cd os-core10
sudo tar -xz --acls --xattrs-include='*' -f base.sfs.cache.tar.gz
cd ..
```

Build base.sfs and initrd images with
```bash
cd os-core10
sudo ~/.local/bin/mkosi -i -f 2>&1 | tee "build-logs/build_$(date +'%Y-%m-%d_%H:%M').log"
```
Finally run
```bash
sudo ./create_deb.sh
```
to create deb file.

### Building from scratch
If you want to add, remove or update Debian and or Python packages you can ignore the cached images and rebuild them from scratch. This will take significantly more time.

To do so simply run
```bash
cd os-core10
sudo ~/.local/bin/mkosi -i -ff 2>&1 | tee "build-logs/build_$(date +'%Y-%m-%d_%H:%M').log"
```
And finally run
```bash
sudo ./create_deb.sh
```

If you have changed or rebuilt the cache with the mkosi parameter `-ff` please make sure to repack both cache folders before making the commit:
```
sudo tar -cz --acls --xattrs -f base.sfs.cache.tar.gz base.sfs.cache-pre-*
```

Default temp dir is /var/tmp which can be changed by setting the TMPDIR environment variable like in `sudo TMPDIR=/same/fs/as/pwd mkosi...`.

Debian mirror can be set with the `-m` resp. `--mirror` option like in `... mkosi --mirror http://localhost/debian/ ...`.

### GIT LFS
The build of the image depends heavily on external sources like the debian repositories. To achieve reproducibility we need the debian files and the cached images (which contain all unpacked debian and python packages).
Since this would be too big for regular git we use git LFS for:
* mkosi.cache/**
* base.sfs.cache.tar.gz


## Workflow
For a detailed explanation how mkosi works see [https://github.com/systemd/mkosi](https://github.com/systemd/mkosi). The following workflow diagram should help to unterstand the sequence of things:

![alt text](mkosi-workflow.png "Mkosi Workflow Diagram")

### mkosi.prepare
This is a script. It does the following:

  * Download and install pip for python2.7 and python3.7
  * Install python packages with pip
  * Add security/updates repo and install security/updates

### mkosi.postinst
This is a script. It does a lot of stuff:

  * Set default shell to bash
  * Create /etc/motd and /etc/issue
  * Generate locales (de, en)
  * Configure Hostname
  * Configure user tcos
  * Rehash Certificates
  * Run depmod
  * Clean up apt
  * Set permissions for /etc/skel
  * User update-alternativces to set mesa for glx (instead of nvidia)

### mkosi.build
This is a script. It does the following:

  * Copy DKMS built drivers to $DESTDIR
  * Build FreeRDP
  * Build Busybox
  * Create kernel modules image for initrd

### mkosi.finalize
This is a script. It does nothing in the build step. In the final step it does the following:

  * Build initrd.img
  * Create artifacts folder (for deb build)
  * Remove unnecessary files from image
