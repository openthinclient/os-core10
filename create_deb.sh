#!/bin/bash

[ "$EUID" -eq 0 ] || exec sudo "$0" "$@"
cd $(dirname $0)

set -e

# Move base.sfs
cp base.sfs artifacts/base/sfs
cp base.changelog artifacts/

# Create zsync file
zsyncmake base.sfs -u base.sfs -o base.sfs.zsync
cp base.sfs.zsync artifacts/base/sfs

# Create m5sums of all files
(
cd artifacts/base
find . -type f -not -iname "*.md5" -not -wholename "*/DEBIAN/*" | xargs realpath --relative-to . | xargs md5sum | tee DEBIAN/md5sums
)

# Get Version from changelog
VERSION_ORIG=$(dpkg-parsechangelog --show-field=Version -l base.changelog)

# Modify Version if building by CI and not on master or hotfix branch
BRANCH_TYPE=$((git symbolic-ref --short HEAD || echo HEAD) | sed "s/\/.*//")

if [[ "${BRANCH_TYPE}" != "master" ]] && [[ "${BRANCH_TYPE}" != "hotfix" ]]; then
    VERSION="${VERSION_ORIG}~${BRANCH_TYPE}${BUILD_ID}"
else
    VERSION=${VERSION_ORIG}
fi

echo On $BRANCH_TYPE branch
echo Version: $VERSION

sed -i artifacts/base/DEBIAN/changelog -e "s/^base\s\+($VERSION_ORIG)/base ($VERSION)/"
sed -i artifacts/base/DEBIAN/control -e "s/^\(Version:\s\+\).*/\1$VERSION/"

# Rename folder
chmod 755 artifacts/base
mv artifacts/base artifacts/base_${VERSION}_amd64
cd artifacts
dpkg-deb -Zgzip --build base_${VERSION}_amd64
mv base_${VERSION}_amd64 base

if [[ $(id -u) -eq 0 ]]; then
    chown -R $SUDO_UID:$SUDO_GID .
fi
